/* eslint-disable no-sequences */

const auth = {
  isAuthenicated: false,
  user: {},
  authenticate(cb, payload) {
    // eslint-disable-next-line no-unused-expressions
    ((this.isAuthenicated = true), (this.user = payload)), setTimeout(cb, 100);
  },
  signout(cb) {
    // eslint-disable-next-line no-unused-expressions
    ((this.isAuthenicated = false), (this.user = {})), setTimeout(cb, 100);
  }
};

export default auth;
