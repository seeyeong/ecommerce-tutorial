import _ from 'lodash';
import {
  SET_FILTER,
  GET_CATEGORIES_PRODUCTS,
  ADD_TO_CART,
  UPDATE_CART_QTY,
  REMOVE_CART_ITEM,
  SUBMIT_ORDER,
  UPDATE_BADGE,
  AUTH,
  SIGNOUT,
  SELECTED_CART,
  UPDATE_SELECTED_CART,
  UPDATE_SELECTED_CART_QTY,
  UPDATE_SELECTED_CART_KEY
} from '../actionTypes';

const products = require('../../json/products.json');
const initialState = {
  allProducts: products,
  categoriesProduct: [],
  filteredProducts: [],
  menu: [],
  curCategory: { key: 'all' },
  cart: [],
  selectedColor: '',
  order: [],
  count: 0,
  auth: {},
  isAuthenicated: false,
  selectedCart: [],
  selectedTotal: 0,
  selectedCartKey: []
};

const getCartItemKey = (cart, payload) => {
  let materials = {
    id: payload.id,
    selectedColor: payload.selectedColor,
    selectedStorage: payload.selectedStorage
  };
  Object.keys(materials).forEach((key) => materials[key] === undefined && delete materials[key]);
  return _.findIndex(cart, materials);
};

const ecommerce = (state = initialState, { type, payload }) => {
  switch (type) {
    case UPDATE_CART_QTY:
      let key = getCartItemKey(state.cart, payload);
      let updateCart = state.cart.map((item, index) => (index === key ? { ...item, qty: payload.qty } : item));
      return { ...state, cart: updateCart };
    case ADD_TO_CART:
      return { ...state, cart: [ ...state.cart, payload ] };
    case GET_CATEGORIES_PRODUCTS:
      var category = _.groupBy(state.allProducts, 'category');
      let menuArr = [];
      _.forEach(category, (value, key) => {
        _.forEach(value, (v, i) => {
          if (_.has(v, 'subcategory')) {
            category[key] = _.groupBy(value, 'subcategory');
            menuArr.push({
              name: key,
              sub: Object.keys(_.groupBy(value, 'subcategory')).map((o) => {
                return { name: o };
              })
            });
          } else {
            menuArr.push({
              name: key,
              sub: null
            });
          }
        });
      });
      let uniqMenu = _.uniqWith(menuArr, _.isEqual);
      return { ...state, categoriesProduct: category, menu: uniqMenu };
    case SET_FILTER:
      let ky = payload.key;
      let fp = []; //initial a filter product arrray
      const { allProducts, categoriesProduct } = state;
      if (ky === 'all') {
        fp = categoriesProduct;
      } else {
        //use filter to filter product
        let filter = _.filter(allProducts, (o) => {
          return o.category === ky || o.subcategory === ky;
        });
        fp = { [ky]: filter };
      }
      return { ...state, filteredProducts: fp };
    case REMOVE_CART_ITEM:
      let rmkey = getCartItemKey(state.cart, payload);
      let cart = _.filter(state.cart, (item, index) => index !== rmkey);
      return { ...state, cart: cart };
    case SUBMIT_ORDER:
      let order = state.cart;
      return { ...state, cart: [], order: order };
    case UPDATE_BADGE:
      let badge = _.reduce(state.cart, (sum, n) => sum + n.qty, 0);
      return { ...state, count: badge };
    case AUTH:
      setTimeout(payload.cb, 100);
      return { ...state, isAuthenicated: true, auth: payload.user };
    case SIGNOUT:
      setTimeout(payload.cb, 100);
      return { ...state, isAuthenicated: false, auth: {} };
    case SELECTED_CART:
      return { ...state, selectedCart: payload };
    case UPDATE_SELECTED_CART:
      return { ...state, selectedTotal: _.reduce(state.selectedCart, (t, r) => t + r.price * r.qty, 0) };
    case UPDATE_SELECTED_CART_QTY:
      let sk = getCartItemKey(state.selectedCart, payload);
      let updateSelectedCart = state.selectedCart.map(
        (item, index) => (index === sk ? { ...item, qty: payload.qty } : item)
      );
      return { ...state, selectedCart: updateSelectedCart };
    case UPDATE_SELECTED_CART_KEY:
      console.log(UPDATE_SELECTED_CART_KEY, payload);
      return { ...state, selectedCartKey: payload };
    default:
      return state;
  }
};

export default ecommerce;
