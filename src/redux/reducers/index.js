import { combineReducers } from 'redux';
import ecommerce from './ecommerce';

export default combineReducers({
  ecommerce
});
