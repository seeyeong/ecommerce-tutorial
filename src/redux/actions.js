import {
  SET_FILTER,
  GET_CATEGORIES_PRODUCTS,
  ADD_TO_CART,
  UPDATE_CART_QTY,
  REMOVE_CART_ITEM,
  SUBMIT_ORDER,
  UPDATE_BADGE,
  AUTH,
  SIGNOUT,
  SELECTED_CART,
  UPDATE_SELECTED_CART,
  UPDATE_SELECTED_CART_QTY,
  UPDATE_SELECTED_CART_KEY
} from './actionTypes';

export const filter = (payload) => ({
  type: SET_FILTER,
  payload
});

export const categoriesProducts = () => ({
  type: GET_CATEGORIES_PRODUCTS
});

export const addToCart = (payload) => ({
  type: ADD_TO_CART,
  payload
});

export const updateCartQty = (payload) => ({
  type: UPDATE_CART_QTY,
  payload
});

export const removeCartItem = (payload) => ({
  type: REMOVE_CART_ITEM,
  payload
});

export const submitOrder = () => ({
  type: SUBMIT_ORDER
});

export const updateBadge = () => ({
  type: UPDATE_BADGE
});

export const addToCartUpdateBadge = (payload) => {
  return async (dispatch) => {
    await dispatch(addToCart(payload));
    dispatch(updateBadge(payload));
  };
};

export const updateCartQtyUpdateBadge = (payload) => {
  return async (dispatch) => {
    await dispatch(updateCartQty(payload));
    await dispatch(updateBadge(payload));
    dispatch(updateSelectedTotal());
  };
};

export const submitOrderUpdateBadge = () => {
  return async (dispatch) => {
    await dispatch(submitOrder());
    await dispatch(updateBadge());
    dispatch(updateSelectedTotal());
  };
};

export const authenticate = (payload) => ({
  type: AUTH,
  payload
});

export const signout = () => ({
  type: SIGNOUT
});

export const selectedCart = (payload) => ({
  type: SELECTED_CART,
  payload
});

export const updateSelectedCartQty = (payload) => ({
  type: UPDATE_SELECTED_CART_QTY,
  payload
});

export const updateSelectedTotal = () => ({
  type: UPDATE_SELECTED_CART
});

export const selectedCartUpdateSelectTotal = (payload) => {
  return async (dispatch) => {
    await dispatch(selectedCart(payload));
    await dispatch(updateSelectedTotal());
  };
};

export const updateSelectedCartUpdateSelectTotal = (payload) => {
  return async (dispatch) => {
    await dispatch(updateSelectedCartQty(payload));
    await dispatch(updateSelectedTotal());
  };
};

export const updateSelectedCartKey = (payload) => ({
  type: UPDATE_SELECTED_CART_KEY,
  payload
});
