import React from 'react';
import { Home, Login, About, Cart, BadgeComponent, OrderList, Payment, Profile, Hotel,Rooms } from './components';
import App from './App';

const routes = [
  {
    component: App,
    routes: [
      {
        path: '/',
        name: 'Home',
        exact: true,
        component: Home
      },
      {
        path: '/order',
        name: 'Order',
        hide: true,
        component: OrderList
      },
      {
        path: '/about',
        name: 'About',
        component: About
      },
      {
        path: '/Room',
        name: 'Room',
        component: About
      },
      {
        path: '/cart',
        name: <BadgeComponent />,
        component: Cart
      },
      {
        path: '/payment',
        name: 'Payment',
        hide: true,
        component: Payment
      },
      {
        path: '/login',
        name: 'Login',
        hideWithAuth: true,
        component: Login
      },
      {
        path: '/profile',
        // name: 'Profile',
        // hide: true,
        component: Profile
      },
      {
        path: '/hotel',
        name: 'Hotel',
        component: Hotel
      },
      {
        path: '/rooms',
        component: Rooms
      },
    ]
  }
];

export default routes;
