import React, { Component } from 'react';
import './App.css';
import { Layout, Menu, Row, Col, notification, Avatar, Dropdown } from 'antd';
import { BrowserRouter as Router, Link } from 'react-router-dom';
import routes from './route.config';
import _ from 'lodash';
import { renderRoutes } from 'react-router-config';
import { SmileTwoTone } from '@ant-design/icons';
import { connect } from 'react-redux';
import { filter, categoriesProducts, addToCartUpdateBadge, updateCartQtyUpdateBadge, signout } from './redux/actions';

const { Header, Footer, Content } = Layout;

const dropdownMenu = (
  <Menu style={{ marginTop: '20px', width: '100px' }}>
    <Menu.Item>
      <Link to='/profile/'>Profile</Link>
    </Menu.Item>
    <Menu.Item>
      <Link to='/profile/order'>Order</Link>
    </Menu.Item>
  </Menu>
);

class App extends Component {
  constructor() {
    super();
    this.state = {
      badge: 0
    };
  }

  handleAddCart = async (data) => {
    //add count
    if (!data.qty) {
      console.log('item qty is empty or zero');
      notification.open({
        message: 'Item invalid add to cart',
        description: 'You need to add quantity, select color or storage.',
        icon: <SmileTwoTone />,
        onClick: () => {
          console.log('Notification Clicked!');
        }
      });
    } else {
      //if product item is existing in cart add qty only
      let materials = { id: data.id, selectedColor: data.selectedColor, selectedStorage: data.selectedStorage };
      Object.keys(materials).forEach((key) => materials[key] === undefined && delete materials[key]);
      //first check id is existing in cart
      let find = _.find(this.props.cart, materials);
      if (find) {
        let newData = {
          ...find,
          qty: find.qty + data.qty
        };
        await this.props.updateCartQtyUpdateBadge(newData);
      } else {
        await this.props.addToCartUpdateBadge(data);
      }
    }
  };

  componentDidMount() {
    this.props.categoriesProducts();
    this.props.filter({ type: 'SET_FILTER', key: 'all' });
    console.log(this.props);
  }

  handleOnClick = (props) => {
    props.signout();
  };

  render() {
    return (
      <Router>
        <Layout className='layout'>
          <Header style={{ position: 'fixed', zIndex: 1, width: '100vw', height: '10vh' }}>
            <div style={{ width: '90%', margin: '0 auto' }}>
              <Link to='/'>
                <div className='logo' />
              </Link>
              <Menu
                theme='dark'
                mode='horizontal'
                defaultSelectedKeys={[ '2' ]}
                style={{ lineHeight: '10vh', float: 'right' }}
              >
                {routes[0].routes.map((route, index) => {
                  return route.hideWithAuth
                    ? !this.props.isAuthenticard && (
                        <Menu.Item key='avatar'>
                          <Dropdown overlay={dropdownMenu}>
                            <Avatar
                              key='avatar'
                              src='https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png'
                              // onClick={console.log('avatar')}
                            />
                          </Dropdown>
                        </Menu.Item>
                      )
                    : !route.hide && (
                        <Menu.Item key={route.path}>
                          <Link to={route.path} className='nav-text'>
                            {route.name}
                          </Link>
                        </Menu.Item>
                      );
                })}
                <Menu.Item key='5'>
                  <Link to='/cart' className='nav-text' />
                </Menu.Item>
              </Menu>
            </div>
          </Header>
          <Content style={{ width: '80%', minHeight: '80vh', margin: '0 auto', marginTop: '10vh' }}>
            <Row>
              <Col className='main-content'>
                {renderRoutes(routes[0].routes, { ...this.props, handleAddCart: this.handleAddCart })}
              </Col>
            </Row>
          </Content>
          <Footer style={{ textAlign: 'center' }}>Ant Design ©2018 Created by Ant UED</Footer>
        </Layout>
      </Router>
    );
  }
}

const mapStateToProps = (state) => state.ecommerce;

const mapDispatchToProps = (dispatch) => ({
  filter: (payload) => dispatch(filter(payload)),
  categoriesProducts: () => dispatch(categoriesProducts()),
  signout,
  addToCartUpdateBadge: (payload) => dispatch(addToCartUpdateBadge(payload)),
  updateCartQtyUpdateBadge: (payload) => dispatch(updateCartQtyUpdateBadge(payload))
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
