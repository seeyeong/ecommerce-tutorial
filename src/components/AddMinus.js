import React from 'react';
import { Button, Tooltip, Row, Col } from 'antd';
import { MinusOutlined, PlusOutlined } from '@ant-design/icons';

export default function AddMinus(props) {
  const { handleDecrement, handleIncrement } = props;
  return (
    <Row justify={props.justify ? props.justify : 'center'} style={props.style ? props.style : { textAlign: 'center' }}>
      <Col span={4}>
        <Tooltip>
          <Button size='small' shape='circle' icon={<MinusOutlined />} onClick={handleDecrement} />
        </Tooltip>
      </Col>
      <Col span={8}>{props.qty}</Col>
      <Col span={4}>
        <Tooltip>
          <Button size='small' shape='circle' icon={<PlusOutlined />} onClick={handleIncrement} />
        </Tooltip>
      </Col>
    </Row>
  );
}
