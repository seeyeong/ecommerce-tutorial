import React from 'react';
import { Carousel } from 'antd';
import '../css/Carousel.css';

export default function CarouselComponent() {
  return (
    <Carousel autoplay className='customCarousel'>
      <div>
        <img
          className='carouselImg'
          alt='example'
          src='https://cnet4.cbsistatic.com/img/bsjJ-VWnc9gbcP1r0vKPvyPuk_k=/1092x0/2019/10/31/869a4600-3608-412c-8c7e-2f1f8424b955/apple-iphone-11-comparison-2.jpg'
        />
      </div>
      <div>
        <img
          className='carouselImg'
          alt='example'
          src='https://blog.daraz.com.bd/wp-content/uploads/2019/10/iphone-11-banner.png'
        />
      </div>
      <div>
        <img
          className='carouselImg'
          alt='example'
          src='https://images.macrumors.com/article-new/2019/07/applemusiconemonthbanner-800x359.jpg'
        />
      </div>
      <div>
        <img
          className='carouselImg'
          alt='example'
          src='https://i.insider.com/5c097b361486fd04cf3ba8a6?width=1100&format=jpeg&auto=webp'
        />
      </div>
    </Carousel>
  );
}
