import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Card, Form, Input, Row, Col, Checkbox } from 'antd';
import TextArea from 'antd/lib/input/TextArea';

const layout = {
  labelCol: {
    span: 4
  },
  wrapperCol: {
    span: 20
  }
};

const tailLayout = {
  wrapperCol: {
    offset: 4,
    span: 20
  }
};

class BillingForm extends Component {
  formRef = React.createRef();
  render() {
    return (
      <div>
        <Row>
          <Col>
            <h1>Delivery Information</h1>
            <Card>
              <Form
                {...layout}
                ref={this.formRef}
                name='basic'
                initialValues={{
                  remember: true
                }}
                onFinish={this.onFinish}
              >
                <Form.Item
                  label='Name'
                  name='name'
                  rules={[
                    {
                      required: true,
                      message: 'Please input your username!'
                    }
                  ]}
                >
                  <Input />
                </Form.Item>
                <Form.Item
                  label='Phone'
                  name='phone'
                  rules={[
                    {
                      required: true,
                      message: 'Please input your phone number!'
                    }
                  ]}
                >
                  <Input />
                </Form.Item>
                <Form.Item
                  label='Address'
                  name='address'
                  rules={[
                    {
                      required: true,
                      message: 'Please input your address!'
                    }
                  ]}
                >
                  <TextArea row={4} />
                </Form.Item>
              </Form>
            </Card>
          </Col>
        </Row>
        <Row>
          <Col>
            <h1>Billing Information</h1>
            <Card>
              <Form
                {...layout}
                ref={this.formRef}
                name='billingform'
                initialValues={{
                  remember: true
                }}
                onFinish={this.onFinish}
              >
                <Form.Item {...tailLayout}>
                  <Checkbox>Same as delivery information</Checkbox>
                </Form.Item>
                <Form.Item
                  label='Name'
                  name='name'
                  rules={[
                    {
                      required: true,
                      message: 'Please input your username!'
                    }
                  ]}
                >
                  <Input />
                </Form.Item>
                <Form.Item
                  label='Phone'
                  name='phone'
                  rules={[
                    {
                      required: true,
                      message: 'Please input your phone number!'
                    }
                  ]}
                >
                  <Input />
                </Form.Item>
                <Form.Item
                  label='Address'
                  name='address'
                  rules={[
                    {
                      required: true,
                      message: 'Please input your address!'
                    }
                  ]}
                >
                  <TextArea row={4} />
                </Form.Item>
              </Form>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({});

export default connect(mapStateToProps)(BillingForm);
