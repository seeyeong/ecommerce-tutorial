import React from 'react';
import { connect } from 'react-redux';

import { Row, Col, Descriptions, Badge, Card, Table, Typography } from 'antd';
import DescriptionsItem from 'antd/lib/descriptions/Item';
import NumberFormat from 'react-number-format';

const { Text } = Typography;

function Order(props) {
  const columns = [
    {
      title: 'Item',
      dataIndex: 'title',
      key: 'item'
      // render: text => <a>{text}</a>,
    },
    {
      title: 'Description',
      dataIndex: 'description',
      ellipsis: true,
      key: 'description'
    },
    {
      title: 'Selection',
      key: 'selection',
      render: (text, record) => (
        <div>
          <Row>
            <Col>{record.selectedColor}</Col>
          </Row>
          <Row>
            <Col>{record.selectedStorage}</Col>
          </Row>
        </div>
      )
    },
    {
      title: 'Qty',
      dataIndex: 'qty',
      key: 'qty'
    },
    {
      title: 'Subtotal',
      key: 'subtotal',
      render: (text, record) => (
        <NumberFormat
          value={record.qty * record.price}
          displayType={'text'}
          thousandSeparator={true}
          prefix={`${record.currency} `}
        />
      ),
      className: 'column-subtotal'
    }
  ];
  return (
    <Row>
      <Col>
        <Card>
          <Descriptions title='User Order' bordered>
            <DescriptionsItem label='Order ID'>#12873872</DescriptionsItem>
            <Descriptions.Item label='Payment method' span={2}>
              Credit Card
            </Descriptions.Item>
            <Descriptions.Item label='Order time'>2018-04-24 18:00:00</Descriptions.Item>
            <Descriptions.Item label='Usage Time' span={2}>
              2019-04-24 18:00:00
            </Descriptions.Item>
            <Descriptions.Item label='Address' span={3}>
              Data disk type: MongoDB
              <br />
              Database version: 3.4
              <br />
              Package: dds.mongo.mid
              <br />
              Storage space: 10 GB
              <br />
              Replication factor: 3
              <br />
              Region: East China 1<br />
            </Descriptions.Item>
            <Descriptions.Item label='Status'>
              <Badge status='success' text='Paid' />
            </Descriptions.Item>
          </Descriptions>
        </Card>
      </Col>
      <Col>
        <Card>
          <Table
            pagination={false}
            columns={columns}
            dataSource={props.order}
            rowKey='id'
            summary={(pageData) => {
              let total = 0;

              pageData.forEach(({ qty, price }) => {
                total += qty * price;
              });

              return (
                <tr>
                  <th>Total</th>
                  <td colSpan={4} style={{ textAlign: 'right' }}>
                    <Text>
                      <NumberFormat value={total} displayType={'text'} thousandSeparator={true} />
                    </Text>
                  </td>
                </tr>
              );
            }}
          />
        </Card>
      </Col>
    </Row>
  );
}

const mapStateToProps = (state) => state.ecommerce;

export default connect(mapStateToProps)(Order);
