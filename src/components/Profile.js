import React from 'react';
import { Row, Col, Menu, Form, Card, Input, Select, Button } from 'antd';
import { Switch, Route, useParams, useRouteMatch, Link } from 'react-router-dom';

const NestedProfile = () => {
  let { topicId } = useParams();
  return <h1>{topicId}</h1>;
};

const layout = {
  labelCol: {
    span: 6
  },
  wrapperCol: {
    span: 18
  }
};

const tailLayout = {
  wrapperCol: { offset: 6, span: 18 }
};

const { Option } = Select;

const states = [
  { short: 'JHR', name: 'Johor' },
  { short: 'KDH', name: 'Kedah' },
  { short: 'KTN', name: 'Kelantan' },
  { short: 'MLK', name: 'Melaka' },
  { short: 'NSN', name: 'Negeri Sembilan' },
  { short: 'PHG', name: 'Pahang' },
  { short: 'PRK', name: 'Perak' },
  { short: 'PLS', name: 'Perlis' },
  { short: 'PNG', name: 'Pulau Pinang' },
  { short: 'SBH', name: 'Sabah' },
  { short: 'SWK', name: 'Sarawak' },
  { short: 'SGR', name: 'Selangor' },
  { short: 'TRG', name: 'Terengganu' },
  { short: 'KUL', name: 'W.P. Kuala Lumpur' },
  { short: 'LBN', name: 'W.P. Labuan' },
  { short: 'PJY', name: 'W.P. Putrajaya' }
];

export default function Profile() {
  let { path, url } = useRouteMatch();
  return (
    <Row gutter={16}>
      <Col span={4}>
        <Menu defaultSelectedKeys={[ '0' ]}>
          <Menu.Item key='0'>
            <Link to={`${url}`}>Profile</Link>
          </Menu.Item>
          <Menu.Item key='1'>
            <Link to={`${url}/account`}>Account</Link>
          </Menu.Item>
          <Menu.Item key='2'>
            <Link to={`${url}/order`}>Order</Link>
          </Menu.Item>
        </Menu>
      </Col>
      <Col span={20}>
        <Switch>
          <Route exact path={path}>
            <h1>profile</h1>
            <Card>
              <Form
                {...layout}
                name='billingform'
                initialValues={{
                  remember: true
                }}
              >
                <Form.Item label='Name' name='name'>
                  <Input />
                </Form.Item>
                <Form.Item label='Current Password' name='current-password'>
                  <Input.Password />
                </Form.Item>
                <Form.Item label='Password' name='password'>
                  <Input.Password />
                </Form.Item>
                <Form.Item label='Confirm Password' name='confirm-password'>
                  <Input.Password />
                </Form.Item>
                <Form.Item label='Address'>
                  <Input.Group compact>
                    <Form.Item
                      name={[ 'address', 'province' ]}
                      // noStyle
                      rules={[ { required: true, message: 'Province is required' } ]}
                    >
                      <Select style={{ width: '100%' }} placeholder='Select state'>
                        {states.map((state) => <Option value={state.short}>{state.name}</Option>)}
                      </Select>
                    </Form.Item>
                    <Form.Item
                      name={[ 'address', 'street' ]}
                      // noStyle

                      rules={[ { required: true, message: 'Street is required' } ]}
                    >
                      <Input style={{ width: '100%' }} placeholder='Input street' />
                    </Form.Item>
                  </Input.Group>
                </Form.Item>
                <Form.Item label='Phone'>
                  <Input.Group compact>
                    <Form.Item
                      name={[ 'phone', 'countryCode' ]}
                      noStyle
                      style={{ width: '30%' }}
                      rules={[ { required: true, message: 'Coutry code is required' } ]}
                    >
                      <Input style={{ width: '30%' }} />
                    </Form.Item>
                    <Form.Item
                      name={[ 'phone', 'phone' ]}
                      noStyle
                      rules={[ { required: true, message: 'Street is required' } ]}
                    >
                      <Input style={{ width: '70%' }} placeholder='12345678' />
                    </Form.Item>
                  </Input.Group>
                </Form.Item>
                <Form.Item {...tailLayout}>
                  <Button type='primary' htmlType='submit'>
                    Update
                  </Button>
                </Form.Item>
              </Form>
            </Card>
          </Route>
          <Route path={`${path}/:topicId`}>
            <NestedProfile />
          </Route>
        </Switch>
      </Col>
    </Row>
  );
}
