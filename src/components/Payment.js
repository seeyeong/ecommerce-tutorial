import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Steps, Card, Button, message, Row, Col } from 'antd';
import { LoadingOutlined, SmileOutlined } from '@ant-design/icons';
import BillingForm from './BillingForm';

const { Step } = Steps;

const steps = [
  {
    title: 'Delivery & Billing',
    content: <BillingForm />
  },
  {
    title: 'Pay',
    content: 'Pay',
    icon: <LoadingOutlined />
  },
  {
    title: 'Done',
    content: 'Last-content',
    icon: <SmileOutlined />
  }
];

class Payment extends Component {
  constructor() {
    super();
    this.state = {
      current: 0
    };
  }

  componentDidMount() {
    console.log('props', this.props);
  }

  next() {
    const current = this.state.current + 1;
    this.setState({ current });
  }

  prev() {
    const current = this.state.current - 1;
    this.setState({ current });
  }

  render() {
    const { current } = this.state;
    return (
      <Card>
        <Steps current={current}>
          {steps.map((item) => <Step key={item.title} title={item.title} icon={item.icon} />)}
        </Steps>
        <Row style={{ minHeight: '50vh' }}>
          <Col>{steps[current].content}</Col>
        </Row>
        <div className='steps-action'>
          {current > 0 && (
            <Button style={{ margin: 8 }} onClick={() => this.prev()}>
              Previous
            </Button>
          )}
          {current < steps.length - 1 && (
            <Button type='primary' onClick={() => this.next()}>
              Next
            </Button>
          )}
          {current === steps.length - 1 && (
            <Button type='primary' onClick={() => message.success('Processing complete!')}>
              Done
            </Button>
          )}
        </div>
      </Card>
    );
  }
}

const mapStateToProps = (state) => state.ecommerce;

export default connect(mapStateToProps)(Payment);
