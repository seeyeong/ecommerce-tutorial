/* eslint-disable jsx-a11y/alt-text */
import React, { Component } from 'react'
import { Button,Row, Col } from 'antd'
import { Redirect } from 'react-router-dom'


const hotels = [
  {
    name: 'Iconic Penang', 
    imgUrl: "https://pix5.agoda.net/hotelImages/1214427/-1/97c930985d210f4327c927238eb6f937.jpg?s=450x450", 
    rooms: [
      {
        name: "single room",
        max_qty: 10
      },
      {
        name: "double room",
        max_qty: 10
      },
      {
        name: "deluxe room",
        max_qty: 5
      },

    ]
  },
  {
    name: 'Grand Lexus Johor', 
    imgUrl: "https://pix5.agoda.net/hotelImages/1214427/-1/97c930985d210f4327c927238eb6f937.jpg?s=450x450", 
    rooms: [
      {
        name: "single room",
        max_qty: 10
      }
    ]
  }
]

export default class Hotel extends Component {

  constructor() {
    super()
    this.state = {
      redirect : false,
      hotel: {}
    }
  }

  handleRedirect = (hotel) => {
    console.log("redirect")
    this.setState({
      redirect: true,
      hotel: hotel
    })
  }

  render() {

    return (
      <div>
        <h1>Hotel</h1>
        {this.state.redirect && <Redirect to={{
            pathname: '/rooms',
            state: this.state
        }} />}
        {
          hotels.map((hotel) => {
            return <Row>
            <Col><img src={hotel.imgUrl} /></Col>
          <Col>{hotel.name}</Col>
            <Col><Button type='primary' onClick={() => this.handleRedirect(hotel)}>Reserve</Button></Col>
          </Row>
          })
        }    
        
      </div>
    )
  }
}
