/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Row, Col, List, Card, Skeleton } from 'antd';

export class OrderList extends Component {
  render() {
    return (
      <Row>
        <Col>
          <Card>
            <List
              itemLayout='horizontal'
              dataSource={this.props.order}
              renderItem={(item) => (
                <List.Item actions={[ <a key='list-loadmore-edit'>edit</a>, <a key='list-loadmore-more'>more</a> ]}>
                  <Skeleton avatar title={false} loading={item.loading} active>
                    <List.Item.Meta title={item.title} description={item.description} />
                    <div>content</div>
                  </Skeleton>
                </List.Item>
              )}
            />
          </Card>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = (state) => state.ecommerce;

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(OrderList);
