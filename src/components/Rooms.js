import React, { Component } from 'react'
import { Row, Col, Button, Select } from 'antd'

const { Option } = Select;

export default class Rooms extends Component {

  componentDidMount() {
    console.log("Rooms props", this.props)
  }

  render() {
    const {hotel} = this.props.location.state
    return (
      <div>
        <h1>{hotel.name}</h1>
        {
          hotel.rooms.map(room => <Row key={room.name}>
            <Col>{room.name}</Col>
            <Col><Select defaultValue={0}>
              {
                [...Array(room.max_qty+1).keys()].map(qty => <Option key={qty} value={qty}>{qty}</Option>)
              }
              </Select></Col>
            <Col><Button type='primary'>Book Now</Button></Col>
          </Row>)
        }
        
      </div>
    )
  }
}
