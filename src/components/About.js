import React, { Component } from 'react';
import { Row, Col, Card } from 'antd';
import { Redirect } from 'react-router-dom';

class About extends Component {
  constructor(props) {
    super(props);
    this.state = {
      redirect: false,
      item: {}
    };
  }

  handleClick = (id, name, studentid) => {
    console.log('id:%s name:%s studentid:%s', id, name, studentid);
    this.setState({
      redirect: true,
      item: { id, name, studentid }
    });
  };

  render() {
    const { redirect } = this.state;

    if (redirect) {
      return (
        <Redirect
          to={{
            pathname: '/',
            state: this.state.item
          }}
        />
      );
    }

    return (
      <div>
        <Row>
          <Col>
            <Card>
              <h1>About</h1>
              <Row style={{ maxHeight: '200px' }}>
                <Col span={4} style={{ height: '200px' }}>
                  <img
                    style={{ width: '100%', height: '100%' }}
                    alt={'example'}
                    src={'https://os.alipayobjects.com/rmsportal/QBnOOoLaAfKPirc.png'}
                  />
                </Col>
                <Col span={20} style={{ padding: '10px' }}>
                  <h1>What is Lorem Ipsum?</h1>
                  <p>
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the
                    industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type
                    and scrambled it to make a type specimen book. It has survived not only five centuries, but also the
                    leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s
                    with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop
                    publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                  </p>
                </Col>
              </Row>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default About;
