/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter, Redirect } from 'react-router-dom';
import { Row, Col, Table, Popconfirm, message, Button, Typography } from 'antd';
import AddMinus from './AddMinus';
import {
  removeCartItem,
  submitOrder,
  updateCartQtyUpdateBadge,
  selectedCartUpdateSelectTotal,
  updateSelectedCartUpdateSelectTotal,
  updateSelectedTotal,
  updateSelectedCartKey
} from '../redux/actions';
import update from 'immutability-helper';
import { ShoppingOutlined } from '@ant-design/icons';
import '../css/Cart.css';
import NumberFormat from 'react-number-format';

const pagination = false;
const { Title, Text } = Typography;

class Cart extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pagination,
      loading: false,
      redirect: false,
      selectedItems: [],
      selectedTotal: 0
    };
    this.columns = [
      {
        title: '#',
        key: 'img',
        width: 100,
        render: (text, record) => (
          <img style={{ width: '100%', height: 'auto', margin: '0 auto' }} alt={record.title} src={record.imgUrl} />
        )
      },
      {
        title: 'Item',
        dataIndex: 'title',
        key: 'item',
        textWrap: 'word-break',
        width: 200
      },
      {
        title: 'Selection',
        key: 'selection',
        render: (text, record) => (
          <div>
            <Row>
              <Col>{record.selectedColor}</Col>
            </Row>
            <Row>
              <Col>{record.selectedStorage}</Col>
            </Row>
          </div>
        )
      },
      {
        title: 'Qty',
        dataIndex: 'qty',
        key: 'qty',
        render: (qty, record) => (
          <AddMinus
            qty={qty}
            handleDecrement={() => this.handleDecrement(record)}
            handleIncrement={() => this.handleIncrement(record)}
          />
        )
      },
      {
        title: 'Subtotal',
        key: 'subtotal',
        render: (text, record) => (
          <NumberFormat
            value={record.qty * record.price}
            displayType={'text'}
            thousandSeparator={true}
            // prefix={`${record.currency} `}
          />
        ),
        className: 'column-subtotal'
      },
      {
        title: 'Action',
        key: 'action',
        render: (text, record) => (
          <Popconfirm
            title='Are you sure delete this item?'
            onConfirm={(e) => this.handleDelete(record, e)}
            okText='Yes'
            cancelText='No'
          >
            <a>Delete</a>
          </Popconfirm>
        )
      }
    ];
  }

  componentDidMount() {
    this.props.updateSelectedTotal();
  }

  handleDelete(record, e) {
    e.preventDefault();
    this.props.removeCartItem(record);
    message.success('Item has been remove.');
  }

  async handleDecrement(record) {
    if (record.qty > 1) {
      let newRecord = update(record, { qty: { $set: record.qty - 1 } });
      await this.props.updateCartQtyUpdateBadge(newRecord);
      await this.props.updateSelectedCartUpdateSelectTotal(newRecord);
    } else {
      message.error('You need at least 1 qty!');
    }
  }

  async handleIncrement(record) {
    let newRecord = update(record, { qty: { $set: record.qty + 1 } });
    await this.props.updateCartQtyUpdateBadge(newRecord);
    await this.props.updateSelectedCartUpdateSelectTotal(newRecord);
  }

  handleSubmit = async () => {
    this.setState({
      loading: true
    });
    // this.props.history.push(`/payment`);
    this.handlePromise().then(() =>
      this.setState({
        loading: false,
        redirect: true
      })
    );
  };

  handlePromise = () =>
    new Promise((resolve, reject) => {
      setTimeout(() => {
        console.log('submited!');
        resolve(true);
      }, 1000);
    });

  render() {
    const { cart, removeCartItem, selectedTotal, selectedCartKey, selectedCart } = this.props;
    const { loading } = this.state;
    const rowSelection = {
      onChange: async (selectedRowKeys, selectedRows) => {
        this.props.updateSelectedCartKey(selectedRowKeys);
        this.props.selectedCartUpdateSelectTotal(selectedRows);
      },
      getCheckboxProps: (record) => ({
        disabled: record.name === 'Disabled User', // Column configuration not to be checked
        name: record.name
      })
    };
    if (this.state.redirect) {
      return <Redirect to='/payment' />;
    }

    return (
      <div>
        <Row>
          <Col>
            <Table
              {...this.state}
              columns={this.columns}
              dataSource={cart}
              removeCartItem={removeCartItem}
              rowKey='id'
              rowSelection={{
                type: 'checkbox',
                ...rowSelection,
                selectedRowKeys: selectedCartKey
              }}
            />
          </Col>
        </Row>
        <Row justify='end' style={{ margin: '10px' }}>
          <Col>
            <Text>
              Selected {selectedCart.length} {selectedCart.length > 1 ? 'items' : 'item'}
            </Text>
          </Col>
          <Col style={{ textAlign: 'right', paddingRight: '10px' }}>
            <Title level={4} type='danger'>
              <NumberFormat
                value={selectedTotal ? selectedTotal : '0.00'}
                displayType={'text'}
                thousandSeparator={true}
              />
            </Title>
          </Col>
          <Col span={4}>
            <Button type='primary' icon={<ShoppingOutlined />} loading={loading} onClick={this.handleSubmit}>
              Submit Order
            </Button>
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (state) => state.ecommerce;
const mapDispatchToProps = (dispatch) => ({
  removeCartItem: (payload) => dispatch(removeCartItem(payload)),
  submitOrder: () => dispatch(submitOrder()),
  updateCartQtyUpdateBadge: (payload) => dispatch(updateCartQtyUpdateBadge(payload)),
  selectedCartUpdateSelectTotal: (payload) => dispatch(selectedCartUpdateSelectTotal(payload)),
  updateSelectedCartUpdateSelectTotal: (payload) => dispatch(updateSelectedCartUpdateSelectTotal(payload)),
  updateSelectedTotal: () => dispatch(updateSelectedTotal()),
  updateSelectedCartKey: (payload) => dispatch(updateSelectedCartKey(payload))
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Cart));
