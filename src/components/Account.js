import React, { Component } from 'react';
import { connect } from 'react-redux';

export class Account extends Component {
  render() {
    return (
      <div>
        <h1>Account</h1>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(Account);
