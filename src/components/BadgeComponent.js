import React, { Component } from 'react';
import { connect } from 'react-redux';
import { ShoppingCartOutlined } from '@ant-design/icons';
import { Badge } from 'antd';

export class BadgeComponent extends Component {
  render() {
    return (
      <Badge count={this.props.count}>
        <ShoppingCartOutlined size='large' style={{ fontSize: '20px', color: 'white' }} />
      </Badge>
    );
  }
}

const mapStateToProps = (state) => state.ecommerce;

export default connect(mapStateToProps)(BadgeComponent);
