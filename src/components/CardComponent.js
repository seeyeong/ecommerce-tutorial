import React, { Component } from 'react';
import { Card, Button, Row, Col, Radio } from 'antd';
import { ShoppingCartOutlined } from '@ant-design/icons';
import update from 'immutability-helper';
import AddMinus from './AddMinus';
import { connect } from 'react-redux';
import { addToCart } from '../redux/actions';

const { Meta } = Card;

class CardComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      data: this.props.data,
      qty: 0
    };
  }

  handleDecrement = () => {
    if (this.state.qty > 0) {
      let qty = this.state.qty - 1;
      this.setState({
        qty: qty,
        data: update(this.state.data, { qty: { $set: qty } })
      });
    }
  };

  handleIncrement = () => {
    let qty = this.state.qty + 1;
    this.setState({
      qty: this.state.qty + 1,
      data: update(this.state.data, { qty: { $set: qty } })
    });
  };

  handleOnChange = (e) => {
    this.setState({
      data: update(this.state.data, { $merge: { [e.target.name]: e.target.value } })
    });
  };

  render() {
    const { handleAddCart } = this.props;
    const { style, alt, imgUrl, title, description, loading, price, currency, colors, storages } = this.props.data;
    return (
      <div>
        <Card
          key={title}
          hoverable
          style={style && style}
          cover={
            <img
              style={{ width: 'auto', height: '120px', margin: '0 auto' }}
              alt={alt ? alt : 'example'}
              src={imgUrl ? imgUrl : 'https://os.alipayobjects.com/rmsportal/QBnOOoLaAfKPirc.png'}
            />
          }
          loading={loading}
          actions={[
            <Button
              type='primary'
              shape='round'
              icon={<ShoppingCartOutlined />}
              onClick={() => handleAddCart(this.state.data)}
            >
              {currency}
              {price}
            </Button>
          ]}
        >
          <Meta
            title={title ? title : 'Title will be display here'}
            description={description ? description : 'Description goes here'}
          />
          {colors && (
            <Row style={{ margin: 5 }}>
              <Col>
                <Radio.Group buttonStyle='solid' size='small' onChange={this.handleOnChange} name='selectedColor'>
                  {colors.map((color, index) => (
                    <Radio.Button key={index} value={color}>
                      {color}
                    </Radio.Button>
                  ))}
                </Radio.Group>
              </Col>
            </Row>
          )}
          {storages && (
            <Row style={{ margin: 5 }}>
              <Col>
                <Radio.Group buttonStyle='solid' size='small' onChange={this.handleOnChange} name='selectedStorage'>
                  {storages.map((storage, index) => (
                    <Radio.Button key={index} value={storage}>
                      {storage}
                    </Radio.Button>
                  ))}
                </Radio.Group>
              </Col>
            </Row>
          )}

          <Row style={{ margin: 5 }}>
            <Col>
              <AddMinus
                {...this.state}
                handleDecrement={this.handleDecrement}
                handleIncrement={this.handleIncrement}
                justify='start'
              />
            </Col>
          </Row>
        </Card>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({});

const mapDispatchToProps = { addToCart };

export default connect(mapStateToProps, mapDispatchToProps)(CardComponent);
