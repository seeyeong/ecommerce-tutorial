import React from 'react';
import { Row, Col, Affix, Menu } from 'antd';
import CarouselComponent from './CarouselComponent';
import CardComponent from './CardComponent';
import _ from 'lodash';
import { connect } from 'react-redux';
import { filter } from '../redux/actions';

const { SubMenu } = Menu;

function Home(props) {
  const { menu, filteredProducts } = props;
  return (
    <div>
      <Row gutter={24}>
        <Col span={4} className='sidebar'>
          <h1>Shop Name</h1>
          <Affix offsetTop={74}>
            <Menu onClick={(e) => props.filter(e)} mode='vertical'>
              <Menu.Item key='all'>All</Menu.Item>
              {menu.map((menuItem, index) => {
                return _.isArray(menuItem.sub) ? (
                  <SubMenu
                    key={menuItem.name}
                    title={
                      <span>
                        <span>{menuItem.name}</span>
                      </span>
                    }
                  >
                    {menuItem.sub != null &&
                      menuItem.sub.map((subItem, i) => <Menu.Item key={subItem.name}>{subItem.name}</Menu.Item>)}
                  </SubMenu>
                ) : (
                  <Menu.Item key={menuItem.name}>{menuItem.name}</Menu.Item>
                );
              })}
            </Menu>
          </Affix>
        </Col>
        <Col span={20}>
          <Row gutter={[ 16, 16 ]}>
            asdf
            <Col>
              <CarouselComponent />
            </Col>
          </Row>
          <Row gutter={[ 16, 16 ]}>
            {Object.keys(filteredProducts).map((key) => {
              return _.map(filteredProducts[key], function(products, key) {
                return _.isArray(products) ? (
                  products.map((p, i) => {
                    return (
                      <Col key={i} span={8}>
                        <CardComponent data={p} loading={false} style={{ padding: '10px' }} {...props} />
                      </Col>
                    );
                  })
                ) : (
                  <Col key={key} span={8}>
                    <CardComponent data={products} loading={false} style={{ padding: '10px' }} {...props} />
                  </Col>
                );
              });
            })}
          </Row>
        </Col>
      </Row>
    </div>
  );
}

const mapStateToProps = (state) => state.ecommerce;

export default connect(mapStateToProps, { filter })(Home);
